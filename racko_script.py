#!/usr/bin/env python

## 8/2016 Philip Van Stockum

## To use this, first modify the racko_config file to import the player classes which will participate.  
## (No code in this script is intended to be modified by the user.)
## In a python environment such as IDLE:
## Change the working directory to the directory where you have stored this file. (e.g. import os; os.chdir())
## Run this script.
## playMatch(1,['PhilPlayer101','PhilPlayer102'],10,500,0,True)

## I originally had each of the classes in their own modules but had problems with module namespaces.
## The Match module didn't have access to the player classes which were imported into the shell namespace only.
## The player class modules cannot be imported into the various game modules because they change each time.

from racko_player import RackoPlayer
from racko_config import *

import json
import random
import pdb
import copy
import time
import os

savePath = os.getcwd() + '/MatchResults/'

if not os.path.exists(savePath):
    os.makedirs(savePath)

def playMatch(matchId,playerNames,numGames,pointsToWin=500,verbosity=1,saveFlag=False):
    '''
    Play a full Racko match (a bunch of games) between the specified player classes.
    
    Args:
        matchId (int): The id number to save this match under.
        playerNames (list of strings): List of the names of the player classes to compete in this match.  The corresponding modules must have been imported into the Python environment.
        numGames (int): Number of Racko games to play in the match
        pointsToWin (int): Number of points required to win a game.  Defaults to 500.
        verbosity (int): The threshold level at which print output to the console. This affects run times by orders of magnitude. Defaults to 1.
           0: Match statements
           1: Game statments
           2: Round statements
           3: Turn statements
        saveFlag (bool): Whether or not to save the history of the match to a new file in the MatchResults folder. Defaults to False.

    Returns:
        None
    '''

    global savePath
    startTime = time.time()

    #Play the match
    print('Playing match...')
    match = Match(playerNames,numGames,pointsToWin,verbosity)
    matchResults = match.play()
    matchResults['matchId'] = matchId
    
    #Print results
    print('\nMatch %d results:' % matchId)
    for player in matchResults['winCounts']:
        id = player['playerId']
        playTime = next((profile['playTime'] for profile in matchResults['playTimes'] if profile["playerId"] == id))
        print(' %s: %d games won, %0.2f seconds taken' % (player['playerName'],player['wins'],playTime) )

    if saveFlag is True:
        #Save results to file
        filename = '%sMatch_%s_results.txt' % (savePath,matchId)
        with open(filename, 'w') as outfile:
            json.dump(matchResults, outfile, indent=4, sort_keys=True)
        print('Saved results to:')
        print(filename)

    endTime = time.time()
    elapsedSeconds = endTime - startTime
    print('Run time: %0.2f s' % elapsedSeconds)

def loadResults(matchNum):
    global savePath
    with open('%sMatch_%d_results.txt'%(savePath,matchNum)) as data_file:    
        data = json.load(data_file)
    print('keys:')
    for key in data.keys():
        print(key)
    return data

class Match(object):
    def __init__(self,playerNames,numGames,pointsToWin,verbosity): #playerNames is a list of the string names of the classes, which should have been imported in the scope instantiating this class
        self.verbosity = verbosity
        self.playerNames = playerNames
        self.numGames = numGames
        self.pointsToWin = pointsToWin
        #Populate self.playerProfiles (passed to Game) and self.winCounts and self.playTimes (returned along with historical lists)
        self.playerProfiles = []
        self.winCounts = []
        self.playTimes = []
        for n, playerName in enumerate(playerNames):
            self.playerProfiles.append({'playerId':n,'playerName':playerName,'playerClass':eval(playerName)}) #This associates ids with classes.  Does eval work on classes?
            self.winCounts.append({'playerId':n,'playerName':playerName,'wins':0})
            self.playTimes.append({'playerId':n,'playerName':playerName,'playTime':0})
        '''
        winCounts (list)
          player (dict)
              playerId (int)
              wins (int)
        '''

        #Historical lists, elements of which corresponds to games
        self.playerInfo = []
        self.turnHistory = []
        self.pointHistories = []
        self.gameWinnerIds = []
        self.placeHistory = []
        '''
        placeHistory (list)
          game (dict)
              playerId (int)
              place (int)
        '''

    def play(self):
        
        for n in range(self.numGames):
            
            #Instantiate and play game
            if self.verbosity > 0:
                print('\nPlaying game %d...' % n)
            game = Game(self.playerProfiles,self.pointsToWin,self.verbosity) #Is the Game class in scope?
            gameResult = game.play()
            winnerName = next((profile['playerName'] for profile in self.playerProfiles if profile["playerId"] == gameResult['gameWinnerId']))
            if self.verbosity > 0:
                print('Game %d winner: %s' % (n, winnerName))
            
            #Record results
            self.playerInfo.append(gameResult['playerInfo'])
            self.turnHistory.append(gameResult['turnHistory'])
            self.pointHistories.append(gameResult['pointHistories'])
            self.gameWinnerIds.append(gameResult['gameWinnerId'])
            
            #Record places for each player.
            #Sort point histories by final value in descending order
            sortedPointHistories = sorted(gameResult['pointHistories'], key=lambda k: k['pointHistory'][-1], reverse=True)
            #Get a simple list of the sorted ids
            sortedIds = [player['playerId'] for player in sortedPointHistories]
            #Create a list of dictionaries associating player ids with places for this game
            places = [{'playerId':playerId,'place':ind} for ind, playerId in enumerate(sortedIds)]
            #Add to the record of games
            self.placeHistory.append(places)

            #Add to winsCounts
            next((player for player in self.winCounts if player["playerId"] == gameResult['gameWinnerId']))['wins'] += 1
            #Add to playTimes (there must be an easier way)
            ids = [player['playerId'] for player in gameResult['playTimes']]
            playTimes = [player['playTime'] for player in gameResult['playTimes']]
            for n in range(len(ids)): #For each player
                #Update the playTimes
                for m in range(len(self.playTimes)):
                    player = self.playTimes[m]
                    if ids[n] == player['playerId']:
                        player['playTime'] += playTimes[n]
            
        
        #Return all results
        matchResults = {
            'winCounts':self.winCounts,
            'playerInfo':self.playerInfo,
            'turnHistory':self.turnHistory,
            'pointHistories':self.pointHistories,
            'gameWinnerIds':self.gameWinnerIds,
            'placeHistory':self.placeHistory,
            'playTimes':self.playTimes
            }
        return(matchResults)


class Game(object):
    def __init__(self,playerProfiles,pointsToWin,verbosity):
        self.verbosity = verbosity
        self.pointsToWin = pointsToWin
        self.playerProfiles = playerProfiles
        self.playerIds = [] #Reinstantiated in play() after shuffle. Here for bookkeeping purposes.
        self.playerInfo = []
        self.turnHistory = []
        self.playerObjects = []
        self.pointHistories = [] #Reinstantiated in play() after shuffle. Here for bookkeeping purposes.
        self.playTimes = [] #Reinstantiated in play() after shuffle. Here for bookkeeping purposes.
        self.gameWon = False
        self.gameWinnerId = None
        self.numPlayers = None
        '''
        Data structures used:
        
        playerProfiles (list)
          player (dict)
              playerId
              playerClass
        playerInfo (list)
          roundPlayerInfo (list) 
        turnHistory (list)
          roundTurnHistory (list) 
        pointHistories (list)
          player (dict)
              playerId (number)
              pointHistory (list)
                  cumulative points after each round (this list grows by one with each round)
        playTimes (list)
          player (dict)
              playerId (number)
              playTime (number of secands played in this game)
        '''
        
    def play(self):
        # Randomize starting turn order
        random.shuffle(self.playerProfiles)
        #Initiate instance attributes
        self.playerIds = [playerProfile['playerId'] for playerProfile in self.playerProfiles]
        self.playerNames = [playerProfile['playerName'] for playerProfile in self.playerProfiles]
        self.pointHistories = [{'playerId':playerProfile['playerId'],'pointHistory':[0]} for playerProfile in self.playerProfiles]
        self.playTimes = [{'playerId':playerProfile['playerId'],'playTime':0} for playerProfile in self.playerProfiles]
        # Instantiate each player.  It persists for the entire game.
        self.playerObjects = [playerProfile['playerClass'](playerProfile['playerId'],self.playerIds,self.verbosity) for playerProfile in self.playerProfiles]
        self.numPlayers = len(self.playerIds)

        #Play rounds until someone has the winning number of points
        startingPlayerIndex = random.randint(0,self.numPlayers - 1)
        roundNum = 0
        while not self.gameWon:
            roundNum += 1
            
            # Play the round
            if self.verbosity > 1:
                print('Playing round %d...\n' % roundNum)
            startingPlayerIndex = (startingPlayerIndex + 1) % self.numPlayers
            currentRound = Round(self.playerIds,self.playerObjects,startingPlayerIndex,self.verbosity,self.playerNames)
            roundResult = currentRound.play()
            if self.verbosity > 1:
                print('\nRound winner: %s\n\n' % roundResult['roundWinnerName'])

            # Record the results
            self.playerInfo.append(roundResult['playerInfo'])
            self.turnHistory.append(roundResult['turnHistory'])
            
            #Mine the playerInfo for points and add to the appropriate player's entry in self.pointHistories
            #Do the same for playTimes
            #There must be a better way to do this (Modify a dictionary based on the contents of another dictionary with the same keys)
            ids = [player['playerId'] for player in roundResult['playerInfo']]
            playTimes = [player['playTime'] for player in roundResult['playerInfo']]
            points = [player['points'] for player in roundResult['playerInfo']]
            maxPoints = 0 #This will be used to determine the winner of the game
            for n in range(len(ids)): #For each player
                #Update the playTimes
                for m in range(len(self.playTimes)):
                    player = self.playTimes[m]
                    if ids[n] == player['playerId']:
                        player['playTime'] += playTimes[n]
                #Update the pointHistories                       
                for m in range(len(self.pointHistories)):
                    player = self.pointHistories[m]
                    if ids[n] == player['playerId']: #This is the right point history to modify
                        player['pointHistory'].append(player['pointHistory'][-1] + points[n]) #This modifies self.pointHistories                        
                        maxPoints = max(maxPoints,player['pointHistory'][-1])
                        if player['pointHistory'][-1] >= self.pointsToWin: #This player has a winning point total
                            self.gameWon = True
                            if player['pointHistory'][-1] == maxPoints: #This player has the highest winning point total
                                self.gameWinnerId = player['playerId']
                        
        #Sort player type lists by playerId before returning them (they should be in order anyway)
        self.playerProfiles = sorted(self.playerProfiles, key=lambda k: k['playerId'])
        self.pointHistories = sorted(self.pointHistories, key=lambda k: k['playerId'])
        
        return {'playerInfo':self.playerInfo,'turnHistory':self.turnHistory,'pointHistories':self.pointHistories,'gameWinnerId':self.gameWinnerId,'playTimes':self.playTimes}


class Round(object):
    def __init__(self,playerIds,playerObjects,startingPlayerIndex,verbosity,playerNames):
        self.verbosity = verbosity
        self.playerObjects = playerObjects
        self.startingPlayerIndex = startingPlayerIndex
        self.numPlayers = len(playerIds)
        self.playerIds = playerIds
        self.playerNames = playerNames
        self.highestCard = 60 - (4 - self.numPlayers)*10
        self.playerInfo = []
        self.roundWon = False
        self.turnHistory = []
        self.drawPile = []
        self.discardPile = []
        self.roundWinnerId = None
        '''
        Data structures used:
        
        playerInfo(list)
            player (dict)
                    playerId (number)
                    startingRack (list)
                            card
                    currentRack (list)
                            card
                    points
        
        turnHistory (list)
        	turn (dict)
        		playerId
        		pileTaken (number, 0 for down, 1 for up)
        		cardTaken
        		slotChosen (-1 for unused pick)
        		cardDiscarded
        '''
		
    def play(self):
        
        self.deal()
        activePlayerIndex = self.startingPlayerIndex - 1
        while not self.roundWon:
            activePlayerIndex = (activePlayerIndex + 1) % self.numPlayers
            startTime = time.time()
            action = self.giveTurn(activePlayerIndex)
            playTime = time.time() - startTime
            self.processTurn(activePlayerIndex,action,playTime)
        self.processWin()
        return {'playerInfo':self.playerInfo,'turnHistory':self.turnHistory,'roundWinnerId':self.roundWinnerId,'roundWinnerName':self.roundWinnerName}

    def deal(self):

        #Shuffle deck
        deck = list(range(1,self.highestCard+1))
        random.shuffle(deck)

        # Create hands and deal them to players
        for n in range(self.numPlayers):
            playerInfo = {}
            hand = [];
            for m in range(10):
                hand.append(deck.pop())
            id = self.playerIds[n]
            playerName = self.playerNames[n]
            #Populate playerInfo
            playerInfo['playerId'] = id
            playerInfo['playerName'] = playerName
            playerInfo['startingRack'] = copy.copy(hand) #v1.1 update
            playerInfo['currentRack'] = copy.copy(hand) #v1.1 update
            playerInfo['points'] = 0
            playerInfo['playTime'] = 0
            self.playerInfo.append(playerInfo)
            #Deal hands to players
            self.playerObjects[n].receiveHand(hand)
            if self.verbosity > 1:
                print('%s starting rack:' % playerName)
                print(self.playerInfo[n]['currentRack'])

        #Initiate piles
        self.discardPile.append(deck.pop())
        self.drawPile = deck

    def giveTurn(self,activePlayerIndex):
        if self.verbosity > 2:
            print('\n%s turn' % self.playerNames[activePlayerIndex])

        #Prepare info for player
        cardAvailable = self.discardPile[-1]
        lastTurnInfo = copy.deepcopy(self.turnHistory[-(self.numPlayers + 1):-1]) #Copy because are about to modify for presentation only
        #Conceal drawn cards if take from face down draw pile
        for turnInfo in lastTurnInfo:
            if turnInfo['pileTaken'] == 0:
                turnInfo['cardTaken'] = 0
                
        #Give the player its turn
        player = self.playerObjects[activePlayerIndex]
        player.receiveLastTurnInfo(lastTurnInfo)
        pileTaken = player.choosePile(cardAvailable)
        #Determine card to give player
        if pileTaken == 0:
            cardReceived = self.drawPile[-1]
        elif pileTaken == 1:
            cardReceived = self.discardPile[-1]
        slotChosen = player.chooseSlot(cardReceived)
        action = {'pileTaken':pileTaken,'slotChosen':slotChosen}
        return action
        
    def processTurn(self,activePlayerIndex,action,playTime=0):
        if self.verbosity > 2:
            print('old rack:')
            print(self.playerInfo[activePlayerIndex]['currentRack'])
        
        #Add action to turn history after supplementing it with derived info
        playerInfo = self.playerInfo[activePlayerIndex] # Clones the dictionary
        action['playerId'] = playerInfo['playerId']
        if action['pileTaken'] == 0:
            action['cardTaken'] = self.drawPile.pop() # Modifies drawPile
        else:
            action['cardTaken'] = self.discardPile.pop() # Modifies discardPile
        if action['slotChosen'] == -1:
            action['cardDiscarded'] = action['cardTaken']
        else:
            action['cardDiscarded'] = playerInfo['currentRack'][action['slotChosen']]
        self.turnHistory.append(action)

        if self.verbosity > 2:
            print('cardTaken: %d' % action['cardTaken'])
            print('slotChosen: %d' % action['slotChosen'])
        
        #Update player info
        if action['slotChosen'] != -1:
            self.playerInfo[activePlayerIndex]['currentRack'][action['slotChosen']] = action['cardTaken']
            #Make a checksum for ascending cards for debugging
            total = 0
            prevVal = 0
            rack = self.playerInfo[activePlayerIndex]['currentRack']
            for idx, val in enumerate(rack):
                if val > prevVal:
                    total += 10
                    prevVal = val
                else:
                    break
            if self.verbosity > 2:
                print('new rack:')
                print(rack)
                print('Total: %d' % total)
        
        #Add discard to discardPile
        self.discardPile.append(action['cardDiscarded'])
        
        #Check for Racko
        rack = self.playerInfo[activePlayerIndex]['currentRack']
        self.roundWon = all(rack[i] <= rack[i+1] for i in range(len(rack)-1))

        # Reset draw pile if empty
        if len(self.drawPile) == 0:
            self.resetDrawPile()

        #Add playing time for this turn
        self.playerInfo[activePlayerIndex]['playTime'] += playTime
        
    def processWin(self):

        #Award each player points`  
        for player in self.playerInfo:
            previousCard = 0
            n = 0
            points = 0
            while player['currentRack'][n] > previousCard:
                points = points + 5
                if n == 9:
                    break
                previousCard = player['currentRack'][n]
                n = n + 1
            if points == 50: #win
                points = 75
                self.roundWinnerId = player['playerId']
                self.roundWinnerName = player['playerName']
            player['points'] = points

    def resetDrawPile(self):
        
        self.drawPile = list(reversed(self.discardPile))
        self.discardPile = [self.drawPile.pop()] #Modifies drawPile
        
        


