#These player classes are instantiated at the beginnng of each game, persisting through the rounds in that game

# This player starts identifies anchors in its initial rack and then calculates evolving bin limits for each slot by looking at the satisfactory cards surrounding it.
# It does not take into account knowledge of unavailable cards.
# This player differs from Player 101 in that it considers clusters in the initial anchoring.

#There appears to be a problem with the bin limits because rarely this player discards.

import random
import pdb

from racko_player import RackoPlayer

class PhilPlayer102(RackoPlayer):
    
    def __init__(self,playerId,playerIds,verbosity=0):
        '''
        Args:
          playerId (int, your id)
          playerIds (list of int ids of players in game)
          verbosity: (please only allow print statements if this > 2)
            0: Match statements
            1: Game statments
            2: Round statements
            3: Turn statements          

        Returns:
          None
        '''
        
        self.numPlayers = len(playerIds) #Card range depends on number of players
        self.verbosity = verbosity
        self.playerId = playerId
        self.playerIds = playerIds
        self.slotLims = [None]*10
        if self.numPlayers == 4:
            self.highestCard = 60
        elif self.numPlayers == 3:
            self.highestCard = 50
        elif self.numPlayers == 2:
            self.highestCard = 40
        self.binIncrement = self.highestCard / 10
        
    def receiveHand(self,hand):
        '''
        This is called each round.
        
        Args:
          hand (the hand you received for your rack this round.  The first element is your bottom card.)

        Returns:
          None

        '''
        self.rack = hand
        # A satisfied slot is a slot that I don't need to change.
        # Satisfied slots will serve as anchors to build around.
        self.slotsSatisfied = [False]*10
        self.satisfiedCount = 0 #Modify this whenever a slot is satisfied in order to avoid recomputing each turn

        #Iterate calculating slot limits and assigning satisfaction to slots in the limits until convergence of satisfied slots
        #This takes into account clusters.
        satisfactionChanged = True
        while satisfactionChanged:
            self.calcSlotLims()
            satisfactionChanged = self.assignSatisfaction()

    def receiveLastTurnInfo(self, lastTurnInfo):
        '''
        This is called each turn.
        
        Args:
          lastTurnInfo (list the most recent turn for each player)
            turn (dict)
              playerId (num)
              slotChosen (0-9)
              pileTaken (0 for draw pile, 1 for discard pile)
              cardTaken (num, 0 if taken from drawPile)
              cardDiscarded (num)

        Returns:
          None
        '''
        self.lastTurnInfo = lastTurnInfo
    
    def choosePile(self, cardAvailable):
        '''
        This is called each turn after receiveLastTurnInfo returns.
        
        Args:
          cardAvailable (num showing on top of discard pile)

        Returns:
          pileTaken (0 for draw pile, 1 for discard pile)

        '''

        if self.verbosity > 2:
            print(self.slotLims)

        #See if the cardAvailable is inside any of the bin limits.  If so, take it.
        pileTaken = 0
        for lims in self.slotLims:
            if cardAvailable > lims[0] and cardAvailable < lims[1]:
                pileTaken = 1
                break
        
        return pileTaken

    def chooseSlot(self,cardReceived):
        '''
        This is called each turn after choosePile returns.
        
        Args:
          cardReceived (num you drew)

        Returns:
          slotChosen (position in your rack you place drawn card.  0-9, -1 if not using card)

        '''
        slotChosen = -1 #Default to not using the card
        for slot, lims in enumerate(self.slotLims):
            if cardReceived > lims[0] and cardReceived < lims[1]:
                slotChosen = slot
                self.rack[slot] = cardReceived
                self.slotsSatisfied[slot] = True
                self.satisfiedCount += 1
                self.calcSlotLims() #Should this iterate?
                break
            
        return slotChosen


    def assignSatisfaction(self):
        '''
        Assign satisfaction to cards in the rack that are within their slot's bin limits.
        self.slotLims must have been generated before this is run.
        '''
        satisfactionChanged = False
        for slot, card in enumerate(self.rack):
            lims = self.slotLims[slot]
            if card > lims[0] and card < lims[1]:
                self.slotsSatisfied[slot] = True
                self.satisfiedCount += 1
                satisfactionChanged = True
        return satisfactionChanged
        
                

    def calcSlotLims(self):
        '''
        Calculates upper and lower limits (exclusive) for the acceptable card ranges assigned to each slot.
        Range size is 0 for satisfied slots.
        For unsatisfied slots, range is determined by the cards in the bounding satisfied slots.
        '''
        lastSatisfiedSlot = -1
        lastSatisfyingCard = 0
        for slot, card in enumerate(self.rack):
            if self.slotsSatisfied[slot] is True:
                self.slotLims[slot] = [card,card] #null range
                if (slot - lastSatisfiedSlot) > 1:
                    span = (card - lastSatisfyingCard)/(slot - lastSatisfiedSlot - 1)
                    for idx, slot2 in enumerate(range(lastSatisfiedSlot+1,slot)):
                        self.slotLims[slot2] = [lastSatisfyingCard + idx*span, lastSatisfyingCard + (idx +1)*span]
                        #Prevent disqualifying cards that fall at shared borders of adjacent integer spans
                        if idx != 0: #Don't do this for the first item, since it starts from a satisfied number
                            self.slotLims[slot2][0] -= 0.1 #Safe since span never has >8 as a denominator
                lastSatisfiedSlot = slot
                lastSatisfyingCard = card
                
        #Deal with end, including case where no slots are satisfied
        if lastSatisfiedSlot != 9:
            span = (self.highestCard - lastSatisfyingCard)/(10 - lastSatisfiedSlot - 1)
            for idx, slot2 in enumerate(range(lastSatisfiedSlot+1,10)):
                self.slotLims[slot2] = [lastSatisfyingCard + idx*span, lastSatisfyingCard + (idx +1)*span]
                #Prevent disqualifying cards that fall at shared borders of adjacent integer spans
                if idx != 0: #Don't do this for the first item, since it starts from a satisfied number
                    self.slotLims[slot2][0] -= 0.1 #Safe since span never has >8 as a denominator
                #Include highest card
                self.slotLims[slot2][1] += 0.1

        #Floating point errors can give really long numbers, so round to single decimal point
        for slot, card in enumerate(self.rack):
            self.slotLims[slot][0] = round(self.slotLims[slot][0],1)
            self.slotLims[slot][1] = round(self.slotLims[slot][1],1)
