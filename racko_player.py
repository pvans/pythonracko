#Player classes should inherit from this superclass and override all of its methods.
#Player classes are instantiated at the beginnng of each game, with the instances persisting through the rounds in that game

class RackoPlayer(object):
    
    def __init__(self,playerId,playerIds,verbosity=0):
        '''
        Args:
          playerId (int, your id)
          playerIds (list of int ids of players in game)
          verbosity: (please only allow print statements if this > 2)
            0: Match statements
            1: Game statments
            2: Round statements
            3: Turn statements          

        Returns:
          None
        '''
        raise NotImplementedError('Player init method not implemented.')
        
    def receiveHand(self,hand):
        '''
        This is called each round.
        
        Args:
          hand (the hand you received for your rack this round.  The first element is your bottom card.)

        Returns:
          None

        '''
        raise NotImplementedError('Player receiveHand method not implemented.')

    def receiveLastTurnInfo(self, lastTurnInfo):
        '''
        This is called each turn.
        
        Args:
          lastTurnInfo (list the most recent turn for each player)
            turn (dict)
              playerId (num)
              slotChosen (0-9)
              pileTaken (0 for draw pile, 1 for discard pile)
              cardTaken (num, 0 if taken from drawPile)
              cardDiscarded (num)

        Returns:
          None
        '''
        raise NotImplementedError('Player receiveLastTurnInfo method not implemented.')
    
    def choosePile(self, cardAvailable):
        '''
        This is called each turn after receiveLastTurnInfo returns.
        
        Args:
          cardAvailable (num showing on top of discard pile)

        Returns:
          pileTaken (0 for draw pile, 1 for discard pile)

        '''

        raise NotImplementedError('Player choosePile method not implemented.')
        return 0

    def chooseSlot(self,cardReceived):
        '''
        This is called each turn after choosePile returns.
        
        Args:
          cardReceived (num you drew)

        Returns:
          slotChosen (position in your rack you place drawn card.  0-9, -1 if not using card)

        '''
        
        raise NotImplementedError('Player chooseSlot method not implemented.')
        return -1