# README #

Python Racko  
Philip Van Stockum  
2016  
phil (at) abinitioblog (dot) com  

### What is this repository for? ###

* This is a Python simulation of the classic card game Racko.  You can create player classes with arbitrary strategies and play them against each other over large numbers of games to determine superior strategies.
* The rules of the game are [here](https://www.hasbro.com/common/instruct/Racko%281987%29.PDF).
* [More discussion at my blog.](https://dummyurl)

### How do I get set up? ###

You can start by running this program with the sample players included in this repository.
First store the files from this repository in a directory of your choice.
Next, open a Python 3 environment such as IDLE.  In that environment, perform the following steps:

1. `import os`
2. `os.chdir(*STRING CONTAINING THE FULL PATH WHERE YOU HAVE STORED THIS SCRIPT AND THE PLAYER CLASS MODULES*)`
3. Open and run the "racko_script.py" script, for example by using the IDLE graphical menu.  This will load the player classes and game functions.
4. `playMatch(1,['PhilPlayer101','PhilPlayer102'],10,500,0,True)`

Run `help(playMatch)` to learn about its arguments.

The player classes invoked in the above command are imported from their own modules in the python_config.py file. To play the game with players of your own, do the following:

1. Create a new player class in its own module that inherits from the RackoPlayer superclass, which is stored in the racko_player.py module, and overrides all of its methods.  The easiest way to do this may be to copy and modify one of the player classes included in this repository.
2. Add the new player module to the import block in racko_config.py
3. Re-run racko_script.py to load the new module
4. Re-run the `playMatch` function, including the new player class in the playerNames argument.